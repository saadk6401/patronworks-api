﻿using FluentValidation;
using Hangfire;
using Hangfire.Dashboard;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Patronworks.Api.Hubs;
using Patronworks.Api.RouteConstraint;
using Patronworks.Core;
using Patronworks.Core.Attributes;
using Patronworks.Core.Extensions;
using Patronworks.Core.Infrastructure;
using Patronworks.Core.JsonConverters;
using Patronworks.Domain;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model.Administration;
using Patronworks.Domain.Validation;
using Patronworks.Infrastructure;
using Patronworks.Infrastructure.Sieve;
using Sieve.Services;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Patronworks.Api.Infrastructure
{
    public class PatronworksStartup : IPatronworksStartup
    {
        public int Order => 101;

        public void Configure(IApplicationBuilder app)
        {
            app.UseSwagger()
            .UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/v1/swagger.json", "Patronworks.api V1");
                c.OAuthClientId("patronworkswaggerui");
                c.OAuthAppName("patronworks Swagger UI");
            });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions()
            {
                IgnoreAntiforgeryToken = true,
                Authorization = new List<IDashboardAuthorizationFilter>() { new NoAuthFilter() },
                StatsPollingInterval = 60000 //can't seem to find the UoM on github - would love to know if this is seconds or ms

            });
            //app.UseHangfireServer();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<NotificationsHub>("/api/v1/notificationhub", options =>
                {
                    options.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransports.All;
                });
                endpoints.MapHangfireDashboard();
            });
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddCustomDbContext(configuration);

            services.AddSwagger(configuration, "Patronworks", new Dictionary<string, string>() { { "patronworks", "patronworks API" } });

            services.ConfigureAuthService(configuration, "patronworks");

            services.AddScoped<IValidator<Table>>(x => new TableValidator(x.GetRequiredService<ITableService>(), x.GetRequiredService<IStringLocalizer<Table>>()));

            services.AddHangfireServer();

            services.AddScoped<ISieveCustomFilterMethods, PatronworksCustomFileterMethods>();
            services.AddScoped<ISieveCustomSortMethods, PatronworksCustomSortMethods>();
            services.AddScoped<ISieveProcessor, PatronworksSieveProcessor>();
            services.AddHttpClient();
            services.Configure<PatronworksSettings>(configuration);

            services.AddSignalR().AddNewtonsoftJsonProtocol(cfg =>
            {
                cfg.PayloadSerializerSettings.Converters.Add(new JsonConverterObjectToString());
                cfg.PayloadSerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                cfg.PayloadSerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            services.AddRouting(options =>
            {
                options.ConstraintMap.Add("webHookEvent", typeof(WebHookEventRouteConstraint));
            });
        }
    }

    public static class CustomExtensionMethods
    {
        private static string ConnectionString(IConfiguration configuration)
        {
            var conn = $"Server={configuration["POSTGRES_HOSTNAME"]};Port={configuration["POSTGRES_PORT"]};Database={configuration["PATRONWORKS_DATABASE"]};User Id={configuration["POSTGRES_USERNAME"]};Password={configuration["POSTGRES_PASSWORD"]};";
            return conn;
        }
        private static string LocalizationConnectionString(IConfiguration configuration)
        {
            var conn = $"Server={configuration["POSTGRES_HOSTNAME"]};Port={configuration["POSTGRES_PORT"]};Database={configuration["LOCALIZATION_DATABASE"]};User Id={configuration["POSTGRES_USERNAME"]};Password={configuration["POSTGRES_PASSWORD"]};";
            return conn;
        }
        private static string LogConnectionString(IConfiguration configuration)
        {
            var conn = $"Server={configuration["POSTGRES_HOSTNAME"]};Port={configuration["POSTGRES_PORT"]};Database={configuration["LOG_DATABASE"] ?? "Patronworks.Log"};User Id={configuration["POSTGRES_USERNAME"]};Password={configuration["POSTGRES_PASSWORD"]};";
            return conn;
        }
        public static IServiceCollection AddCustomDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            services.AddDbContext<PatronworksDbContext>(options =>
            {
                options.UseNpgsql(ConnectionString(configuration),
                     npgsqlOptionsAction: sqlOptions =>
                     {
                         sqlOptions.MigrationsAssembly(migrationsAssembly);
                         //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                         sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
                         sqlOptions.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                     }).EnableSensitiveDataLogging(true);
            });
            services.AddDbContext<LocalizationDBContext>(options =>
            {
                options.UseNpgsql(LocalizationConnectionString(configuration),
                      npgsqlOptionsAction: sqlOptions =>
                      {
                          sqlOptions.MigrationsAssembly(migrationsAssembly);
                          //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                          sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
                          sqlOptions.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                      });
            });
            services.AddDbContext<LogDbContext>(options =>
            {
                options.UseNpgsql(LogConnectionString(configuration),
                     npgsqlOptionsAction: sqlOptions =>
                     {
                         sqlOptions.MigrationsAssembly(migrationsAssembly);
                         //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                         sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
                         sqlOptions.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                     }).EnableSensitiveDataLogging(true);
            });
            //services.AddDbContext<IntegrationEventLogContext>(options =>
            //{
            //    options.UseNpgsql(ConnectionString(configuration),
            //        npgsqlOptionsAction: sqlOptions =>
            //        {
            //            sqlOptions.MigrationsAssembly(migrationsAssembly);
            //            //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
            //            sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
            //        });
            //});

            services.AddHangfire(config => config.UsePostgreSqlStorage(ConnectionString(configuration)));


            return services;
        }
    }
}
