﻿
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Patronworks.Core.Extensions;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Domain;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model.Administration;
using Patronworks.Domain.Model.Catalog;
using Patronworks.Domain.Model.Payments;
using Patronworks.Infrastructure;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Patronworks.Api.Infrastructure
{
    public class PatronworksContextSeed
    {
        public async Task SeedAsync(PatronworksDbContext context, IHostEnvironment env,
                                    IService<MU> muService, IService<Display> displayService,
                                    IPaymentTypeService paymentTypeService,
                                    IService<PaymentsGType> paymentGTypeService,
                                    ILogger<PatronworksContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            try
            {
                var contentRootPath = env.ContentRootPath;
                await UpdateMu(contentRootPath, muService);
                await UpdateDisplays(contentRootPath, displayService);
                await CreatePaymentTypes(paymentGTypeService, paymentTypeService);
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for LanguageDbContextSeed");

                    await SeedAsync(context, env, muService, displayService, paymentTypeService, paymentGTypeService, logger, retryForAvaiability);
                }
            }
        }
        private async Task UpdateMu(string contentRootPath, IService<MU> muService)
        {
            var mus = (await muService.LoadAll(new SieveModel())).Data;

            string jsonPath = Path.Combine(contentRootPath, "Setup", "mu");
            List<MU> muFromJson = jsonPath.LoadFromJson<MU>();
            var muKeys = mus.Select(f => f.Code);
            var newMus = muFromJson.Where(f => !muKeys.Contains(f.Code));
            if (newMus.Count() > 0)
            {
                await muService.Insert(newMus);
            }
        }
        private async Task UpdateDisplays(string contentRootPath, IService<Display> displayService)
        {
            var displays = (await displayService.LoadAll(new SieveModel())).Data;
            if (displays.Count() == 0)
            {
                string jsonPath = Path.Combine(contentRootPath, "Setup", "displays");
                List<Display> displaysFromJson = jsonPath.LoadFromJson<Display>();
                await displayService.Insert(displaysFromJson);
            }
        }

        private async Task CreatePaymentTypes(IService<PaymentsGType> paymentGTypeService, IPaymentTypeService paymentTypeService)
        {
            var paymentGTypes = (await paymentGTypeService.LoadAll(new SieveModel())).Data.ToList();
            if (paymentGTypes.Count() == 0)
            {
                paymentGTypes = new List<PaymentsGType>();
                paymentGTypes.Add(new PaymentsGType { Code = 1, Name = "Cash" });
                paymentGTypes.Add(new PaymentsGType { Code = 2, Name = "Credit Card" });
                paymentGTypes.Add(new PaymentsGType { Code = 3, Name = "Check" });
                paymentGTypes.Add(new PaymentsGType { Code = 4, Name = "Gift" });
                paymentGTypes.Add(new PaymentsGType { Code = 5, Name = "Debit" });
                paymentGTypes.Add(new PaymentsGType { Code = 6, Name = "Voucher" });
                paymentGTypes.Add(new PaymentsGType { Code = 7, Name = "Account" });
                await paymentGTypeService.Insert(paymentGTypes);
            }
            var paymentTypes = (await paymentTypeService.LoadAll(new SieveModel())).Data.ToList();
            if (paymentTypes.Count() == 0)
            {
                paymentTypes = new List<PaymentType>();
                paymentTypes.Add(new PaymentType
                {
                    Code = "CSH",
                    Name = "Cash",
                    IsActive = true,
                    DefaultPayment = true,
                    ShowCaption = true,
                    PaymentsGType = paymentGTypes.FirstOrDefault(f => f.Code == 1)
                });
                paymentTypes.Add(new PaymentType
                {
                    Code = "CC",
                    Name = "Credit Card",
                    IsActive = true,
                    DefaultPayment = false,
                    ShowCaption = true,
                    PaymentsGType = paymentGTypes.FirstOrDefault(f => f.Code == 2)
                }
                );
                await paymentTypeService.Insert(paymentTypes);
            }
            else
            {
                foreach (var paymentType in paymentTypes)
                {
                    if (!paymentType.PaymentsGTypeId.HasValue)
                    {
                        switch (paymentType.Code)
                        {
                            case "CSH":
                                paymentType.PaymentsGType = paymentGTypes.FirstOrDefault(f => f.Code == 1);
                                break;
                            case "CC":
                                paymentType.PaymentsGType = paymentGTypes.FirstOrDefault(f => f.Code == 2);
                                break;
                        }
                        await paymentTypeService.Update(paymentType);
                    }
                }
            }

        }
        public async Task BeforeMigrationAsync(PatronworksDbContext context, IHostEnvironment env, IOptions<PatronworksSettings> settings, ILogger<PatronworksContextSeed> logger)
        {
            var setting = settings.Value;
            //context.Database.CreateDatabase(setting.POSTGRES_HOSTNAME, setting.POSTGRES_PORT, setting.patronworks_NAME, setting.POSTGRES_USERNAME, setting.POSTGRES_PASSWORD);
            await context.Database.CreateForeignServer("identity_fs", setting.POSTGRES_HOSTNAME, setting.POSTGRES_PORT, setting.IDENTITY_DATABASE, setting.POSTGRES_USERNAME, setting.POSTGRES_PASSWORD);
        }
        //private Policy CreatePolicy(ILogger<PatronworksContextSeed> logger, string prefix, int retries = 3)
        //{
        //    //return Policy.Handle<SqlException>().
        //    //    WaitAndRetry(
        //    //        retryCount: retries,
        //    //        sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
        //    //        onRetry: (exception, timeSpan, retry, ctx) =>
        //    //        {
        //    //            logger.LogTrace($"[{prefix}] Exception {exception.GetType().Name} with message ${exception.Message} detected on attempt {retry} of {retries}");
        //    //        }
        //    //    );
        //}
    }
}
