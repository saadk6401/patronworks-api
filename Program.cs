﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Patronworks.Api.Infrastructure;
using Patronworks.Core.Extensions;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Domain;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model.Administration;
using Patronworks.Domain.Model.Catalog;
using Patronworks.Domain.Model.Payments;
using Patronworks.Infrastructure;
using Serilog;
using Serilog.Events;
using System.IO;

namespace Patronworks.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args)
                //.MigrateDbContext<IntegrationEventLogContext>((_, __) => { })
                .MigrateDbContext<PatronworksDbContext>((context, services) =>
                    {
                        var env = services.GetService<IHostEnvironment>();
                        var logger = services.GetService<ILogger<PatronworksContextSeed>>();
                        var muService = services.GetRequiredService<IService<MU>>();
                        var displayService = services.GetRequiredService<IService<Display>>();
                        var paymentTypeService = services.GetRequiredService<IPaymentTypeService>();
                        var paymentGTypeService = services.GetRequiredService<IService<PaymentsGType>>();
                        new PatronworksContextSeed().SeedAsync(context, env, muService, displayService, paymentTypeService, paymentGTypeService, logger).Wait();
                    },
                    (context, services) =>
                    {
                        var env = services.GetService<IHostEnvironment>();
                        var logger = services.GetService<ILogger<PatronworksContextSeed>>();
                        var settings = services.GetService<IOptions<PatronworksSettings>>();
                        new PatronworksContextSeed().BeforeMigrationAsync(context, env, settings, logger).Wait();

                    })
                .Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
             .UseStartup<Startup>()
                //.UseApplicationInsights()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseWebRoot("Pics")
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    var builtConfig = config.Build();

                    var configurationBuilder = new ConfigurationBuilder();

                    configurationBuilder.AddEnvironmentVariables();

                    config.AddConfiguration(configurationBuilder.Build());
                })
                .ConfigureLogging((hostingContext, builder) =>
                {
                    builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    builder.AddConsole();
                    builder.AddDebug();
                })
                .UseSerilog((builderContext, config) =>
                {
                    config
                        .MinimumLevel.Information()
                        .MinimumLevel.Override("Microsoft.AspNetCore.Hosting.Internal.WebHost", LogEventLevel.Error)
                        .Enrich.FromLogContext()
                        .WriteTo.Console();
                })
                .Build();
    }
}