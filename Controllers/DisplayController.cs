﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Model.Administration;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
namespace Patronworks.Api.Controllers
{
    [Route("api/v1/display")]
    [ApiController]
    [Authorize]
    public class DisplayController : BaseApiController<Display>
    {
        private readonly IService<Display> _displayService;
        public DisplayController(IService<Display> displayService) : base(displayService)
        {
            _displayService = displayService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<Display>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<Display>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            var model = await _service.LoadAll(sieveModel);
            return Ok(model);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(Display), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            var model = await _service.GetById(id);
            return Ok(model);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Display), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(Display model)
        {
            return await base.Post(model);
        }
    }
}