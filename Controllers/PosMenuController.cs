﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.Filters;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model.Administration;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/posmenu")]
    [ApiController]
    [Authorize]
    public class PosMenuController : BaseApiController<PosMenu>
    {
        private readonly IMenuService _menuService;
        public PosMenuController(IMenuService menuService) : base(menuService)
        {
            _menuService = menuService;
        }

        [HttpGet("operatormenu/{categoryId?}")]
        [SwaggerOperationFilter(typeof(ReApplyOptionalRouteParameterOperationFilter))]
        [ProducesResponseType(typeof(PosMenu), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPosMenu(Guid? categoryId = null)
        {
            var model = await _menuService.GetPosMenu(categoryId);
            return Ok(model);
        }
    }
}
