﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Model.Catalog;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/mu")]
    [ApiController]
    [Authorize]
    public class MuController : BaseApiController<MU>
    {
        private readonly IService<MU> _muService;
        public MuController(IService<MU> muService) : base(muService)
        {
            _muService = muService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<MU>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<MU>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            return await base.Get(sieveModel);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(MU), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            return await base.Get(id);
        }


        [HttpPost]
        [ProducesResponseType(typeof(MU), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(MU model)
        {
            return await base.Post(model);
        }
    }
}