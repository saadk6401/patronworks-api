﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model.Catalog;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
namespace Patronworks.Api.Controllers
{
    [Route("api/v1/product")]
    [ApiController]
    [Authorize]
    public class ProductController : BaseApiController<Product>
    {
        private readonly IProductService _productService;
        private readonly IProductPictureService _productPictureService;
        private readonly IImportService _importService;
        public ProductController(IProductService productService,
                                 IProductPictureService productPictureService,
                                 IImportService importService) : base(productService)
        {
            _productService = productService;
            _productPictureService = productPictureService;
            _importService = importService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<Product>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<Product>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            var model = await _service.LoadAll(sieveModel, true, customInclude);
            return Ok(model);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(Product), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            var model = await _service.GetById(id, true);
            return Ok(model);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Product), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(Product model)
        {
            return await base.Post(model);
        }

        [HttpGet]
        [Route("{productId:guid}/picture")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPicture(Guid productId)
        {
            var picture = await _productPictureService.DownloadPicture(productId);
            if (picture == null)
                return NotFound();
            var extension = MimeTypeMap.GetExtension(picture.MimeType);
            Response.Headers.Add("Content-Disposition", new ContentDisposition
            {
                FileName = $"Category_{productId.ToString()}_Media{extension}",
                Inline = true // false = prompt the user for downloading; true = browser to try to show the file inline
            }.ToString());
            return File(picture.PictureBinary, picture.MimeType);
        }

        [HttpPost]
        [Route("{productId:guid}/picture")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostPicture(Guid productId, IFormFile file)
        {
            var stream = file.OpenReadStream();
            await _productPictureService.UploadPicture(productId, stream, file.ContentType);
            return Ok();
        }

        [HttpPatch]
        [Route("{productId:guid}/picture")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PatchPicture(Guid productId, IFormFile file)
        {
            var stream = file.OpenReadStream();
            await _productPictureService.UploadPicture(productId, stream, file.ContentType);
            return Ok();
        }

        [HttpDelete]
        [Route("{productId:guid}/picture")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeletePicture(Guid productId)
        {
            await _productPictureService.DeletePicture(productId);
            return Ok();
        }

        [Route("importproducts")]
        [HttpPost]
        public async Task<IActionResult> ImportProducts(IFormFile xlsxFile, int sheetIndex = 0)
        {
            var result = await _importService.ImportProducts(xlsxFile, sheetIndex);
            var textResult = $"Imported {result} products";
            return Ok(textResult);
        }

        [HttpPatch("{prevId:guid}/changeposition/{currentId:guid}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ChangePosition(Guid prevId, Guid currentId)
        {
            var result = await _productService.ChangeProductPosition(prevId, currentId);
            return Ok(result);
        }
    }
}