﻿using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.Models.Media;
using System;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
namespace Patronworks.Api.Controllers
{
    [Route("api/v1/picture")]
    [ApiController]
    //[Authorize]
    public class PictureController : ControllerBase
    {
        private readonly IService<Picture> _pictureService;
        public PictureController(IService<Picture> pictureService)
        {
            _pictureService = pictureService;
        }

        [HttpGet]
        [Route("{pictureId:guid}")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        [ResponseCache(Duration = 60)]
        public async Task<IActionResult> GetPicture(Guid pictureId)
        {
            var picture = await _pictureService.GetById(pictureId);
            if (picture == null)
                return NotFound();
            var extension = MimeTypeMap.GetExtension(picture.MimeType);
            Response.Headers.Add("Content-Disposition", new ContentDisposition
            {
                FileName = $"Category_{pictureId}_Media{extension}",
                Inline = true // false = prompt the user for downloading; true = browser to try to show the file inline
            }.ToString());
            return File(picture.PictureBinary, picture.MimeType);
        }
    }
}