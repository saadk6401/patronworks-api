﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Domain.Model.Modifiers;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/modifiertemplate")]
    [ApiController]
    [Authorize]
    public class ModifierTemplateController : BaseApiController<ModifierTemplate>
    {
        private readonly IService<ModifierTemplate> _modifierTemplateService;
        public ModifierTemplateController(IService<ModifierTemplate> modifierTemplateService) : base(modifierTemplateService)
        {

        }
    }
}
