﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Model.Payments;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/paymentgtype")]
    [ApiController]
    [Authorize]
    public class PaymentsGTypeController : Controller
    {
        private readonly IService<PaymentsGType> _paymentGTypeService;
        public PaymentsGTypeController(IService<PaymentsGType> paymentGTypeService)
        {
            _paymentGTypeService = paymentGTypeService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<PaymentsGType>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<PaymentsGType>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            var model = await _paymentGTypeService.LoadAll(sieveModel, customInclude: customInclude);
            return Ok(model);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(PaymentsGType), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            var model = await _paymentGTypeService.GetById(id, customInclude: customInclude);
            return Ok(model);
        }


    }
}
