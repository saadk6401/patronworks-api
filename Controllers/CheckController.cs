﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core.Extensions;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model.Checks;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/check")]
    [ApiController]
    [Authorize]
    public class CheckController : ControllerBase
    {
        private readonly ICheckService _checkService;
        private readonly IService<CheckItem> _checkItemService;
        public CheckController(ICheckService checkService, IService<CheckItem> checkItemService)
        {
            _checkService = checkService;
            _checkItemService = checkItemService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<CheckView>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<CheckView>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCheck(DateTime startDate, DateTime endDate, [FromQuery] SieveModel sieveModel)
        {
            if (endDate < startDate)
            {
                return Ok(new PaginatedItemsViewModel<CheckView>());
            }
            if (string.IsNullOrEmpty(sieveModel.Sorts))
            {
                sieveModel.Sorts = "-checkDate";
            }
            endDate = endDate.Date.AddHours(23).AddMinutes(23).AddSeconds(59);
            sieveModel.AddFilter("CheckDates", startDate.Date, FilterOperator.GreaterThanOrEqualTo);
            sieveModel.AddFilter("CheckDates", endDate, FilterOperator.LessThanOrEqualTo);
            var models = await _checkService.LoadCheckView(sieveModel);
            return Ok(models);

        }
        [HttpGet]
        [Route("{checkId:guid}/items")]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<CheckItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<CheckItem>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCheckItems(Guid? checkId, [FromQuery] SieveModel sieveModel)
        {
            if (!checkId.HasValue)
                throw new ArgumentNullException(nameof(checkId));
            if (sieveModel == null)
                sieveModel = new SieveModel();
            sieveModel.AddFilter("CheckId", checkId);
            var models = await _checkItemService.LoadAll(sieveModel, true);
            return Ok(models);
        }
    }
}
