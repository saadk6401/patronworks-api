﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Model.Administration;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/device")]
    [ApiController]
    [Authorize]
    public class DeviceController : BaseApiController<PatronworksDevice>
    {
        private readonly IService<PatronworksDevice> _deviceService;
        public DeviceController(IService<PatronworksDevice> deviceService) : base(deviceService)
        {
            _deviceService = deviceService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<PatronworksDevice>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<PatronworksDevice>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            return await base.Get(sieveModel);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(PatronworksDevice), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            return await base.Get(id);
        }


        [HttpPost]
        [ProducesResponseType(typeof(PatronworksDevice), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(PatronworksDevice model)
        {
            return await base.Post(model);
        }

    }
}