﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Domain.Interfaces.Services;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/common")]
    [ApiController]
    [Authorize]
    public class CommonController : ControllerBase
    {
        private readonly ICommonService _commonService;
        public CommonController(ICommonService commonService)
        {
            _commonService = commonService;
        }
        [HttpDelete("categories")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteCategories()
        {
            var result = await _commonService.DeleteCategories();
            return Ok(result);
        }
        [HttpDelete("products")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteProducts()
        {
            var result = await _commonService.DeleteProducts();
            return Ok(result);
        }

        [HttpDelete("orders")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteOrders()
        {
            var result = await _commonService.DeleteOrders();
            return Ok(result);
        }

        [HttpDelete("all")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> DeleteAll()
        {
            var result = await _commonService.DeleteAll();
            return Ok(result);
        }
    }
}
