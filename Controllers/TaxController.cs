﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Model.Administration;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
namespace Patronworks.Api.Controllers
{
    [Route("api/v1/tax")]
    [ApiController]
    [Authorize]
    public class TaxController : BaseApiController<Tax>
    {
        private readonly IService<Tax> _taxService;
        public TaxController(IService<Tax> taxService) : base(taxService)
        {
            _taxService = taxService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<Tax>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<Tax>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            var model = await _service.LoadAll(sieveModel, true);
            return Ok(model);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(Tax), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            var model = await _service.GetById(id, true);
            return Ok(model);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Tax), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(Tax model)
        {
            return await base.Post(model);
        }
    }
}