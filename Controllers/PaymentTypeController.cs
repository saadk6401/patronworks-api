﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model.Payments;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/paymenttype")]
    [ApiController]
    [Authorize]
    public class PaymentTypeController : BaseApiController<PaymentType>
    {
        private readonly IPaymentTypeService _paymentTypeService;
        public PaymentTypeController(IPaymentTypeService paymentTypeService) : base(paymentTypeService)
        {
            _paymentTypeService = paymentTypeService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<PaymentType>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<PaymentType>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")

        {
            if (string.IsNullOrEmpty(customInclude))
            {
                customInclude = "PaymentsGType";
            }
            return await base.Get(sieveModel, customInclude);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(PaymentType), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            if (string.IsNullOrEmpty(customInclude))
            {
                customInclude = "PaymentsGType";
            }
            return await base.Get(id, customInclude);
        }


        [HttpPost]
        [ProducesResponseType(typeof(PaymentType), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(PaymentType model)
        {
            return await base.Post(model);
        }
    }
}