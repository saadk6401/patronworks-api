﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model;
using System.IO;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/webhook")]
    [ApiController]
    public class WebHookController : Controller
    {
        private readonly IWebHookService _webHookService;
        public WebHookController(IWebHookService webHookService)
        {
            _webHookService = webHookService;
        }

        [HttpPost]
        [Route("{webHookEvent:webHookEvent}")]
        public async Task<IActionResult> WebHook(WebHookEvent webHookEvent)
        {
            var body = await GetBody();
            if (!string.IsNullOrEmpty(body))
            {
                var model = new WebHookModel
                {
                    Event = webHookEvent,
                    Data = body
                };
                await _webHookService.WebHookEventHandler(model);
                return Ok();
            }
            else
                return UnprocessableEntity();
        }

        [HttpPost]
        [Route("dev/{webHookEvent:webHookEvent}")]
        [Authorize]
        public async Task<IActionResult> OpenTable2(string body, WebHookEvent webHookEvent)
        {
            if (!string.IsNullOrEmpty(body))
            {
                var model = new WebHookModel
                {
                    Event = webHookEvent,
                    Data = body
                };
                await _webHookService.WebHookEventHandler(model);
                return Ok();
            }
            else
                return UnprocessableEntity();
        }

        private async Task<string> GetBody()
        {
            try
            {
                var body = await new StreamReader(Request.Body).ReadToEndAsync();
                return body;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
