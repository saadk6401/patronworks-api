﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Patronworks.Core;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.Models;
using Patronworks.Core.ViewModel;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/notification")]
    [ApiController]
    [Authorize]
    public class NotificationController : BaseApiController<PatronworksNotification>
    {
        private readonly IService<PatronworksNotification> _notificationService;
        public NotificationController(IService<PatronworksNotification> notificationService) : base(notificationService)
        {
            _notificationService = notificationService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<PatronworksNotification>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<PatronworksNotification>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            return await base.Get(sieveModel);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(PatronworksNotification), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            return await base.Get(id);
        }


        [HttpPost]
        [ProducesResponseType(typeof(PatronworksNotification), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(PatronworksNotification model)
        {
            return await base.Post(model);
        }



        [HttpPost]
        [Route("table/{tableId:guid}/{deviceId}/{type}")]
        [ProducesResponseType(typeof(PatronworksNotification), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostToDevice(Guid tableId, string deviceId, string type)
        {
            try
            {

                var data = await GetBody();
                if (!string.IsNullOrEmpty(data))
                {
                    dynamic expando = JsonConvert.DeserializeObject<ExpandoObject>(data);
                    var expandoDic = (IDictionary<string, object>)expando;
                    expandoDic.Add("tableId", tableId);
                    expandoDic.Add("deviceId", deviceId);
                    expandoDic.Add("type", type);
                    var jsonData = JsonConvert.SerializeObject(expando);
                    var model = new PatronworksNotification
                    {
                        Data = jsonData
                    };
                    return await base.Post(model);
                }

            }
            catch { }
            return BadRequest();
        }
    }
}