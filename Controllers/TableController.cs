﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model.Administration;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
namespace Patronworks.Api.Controllers
{
    [Route("api/v1/table")]
    [ApiController]
    [Authorize]
    public class TableController : BaseApiController<Table>
    {
        private readonly ITableService _tableService;
        //, IValidator<Table> modelValidator
        public TableController(ITableService tableService) : base(tableService)
        {
            _tableService = tableService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<Table>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<Table>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            var model = await _service.LoadAll(sieveModel, true);
            return Ok(model);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(Table), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            var model = await _service.GetById(id, true);
            return Ok(model);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Table), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(Table model)
        {
            return await base.Post(model);
        }

        [HttpGet]
        [Route("nexttableno")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        public IActionResult GetNextTableNo()
        {
            var nextTableNo = _tableService.GetNextTableNo();
            return Ok(nextTableNo);
        }
    }
}