﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.Enums;
using Patronworks.Core.Extensions;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model;
using Patronworks.Domain.Model.Orders;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/order")]
    [ApiController]
    [Authorize]
    public partial class OrderController : BaseApiController<Order>
    {
        private readonly IOrderService _orderService;
        private readonly IService<OrderItem> _orderItemService;
        public OrderController(IOrderService orderService, IService<OrderItem> orderItemService) : base(orderService)
        {
            _orderService = orderService;
            _orderItemService = orderItemService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<Order>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<Order>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            customInclude = string.IsNullOrEmpty(customInclude) ? "Table" : customInclude;
            return await base.Get(sieveModel, customInclude);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(Order), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            return await base.Get(id, customInclude);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Order), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(Order model)
        {
            return await base.Post(model);
        }


        [HttpDelete("{id:guid}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public override async Task<IActionResult> Delete(Guid id)
        {
            return await base.Delete(id);
        }

        [HttpGet]
        [Route("{orderId:guid}/items")]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<OrderItem>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<OrderItem>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetOrderItems(Guid? orderId, [FromQuery] SieveModel sieveModel)
        {
            if (!orderId.HasValue)
                throw new ArgumentNullException(nameof(orderId));
            bool addOrderId = true;
            if (sieveModel == null)
                sieveModel = new SieveModel();
            if (!string.IsNullOrEmpty(sieveModel.Filters))
            {
                var displayKey = sieveModel.GetFiltersParsed()[0].Values[1];
                if (displayKey == "orders")
                {
                    sieveModel.AddFilter("detailType", OrderDetailType.Notification, FilterOperator.NotEquals);
                    addOrderId = false;
                }
            }
            if (addOrderId)
            {
                sieveModel.AddFilter("orderId", orderId);
            }
            var data = await _orderItemService.LoadAll(sieveModel, true);
            return Ok(data);
        }

        [HttpPost]
        [Route("{orderId:guid}/items")]
        [ProducesResponseType(typeof(OrderItem), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostOrderItem(Guid? orderId, OrderItem model)
        {
            if (!orderId.HasValue)
                throw new ArgumentNullException(nameof(orderId));

            if (ModelState.IsValid)
            {
                model.OrderId = orderId;
                await _orderItemService.Insert(model);
                return Ok(model);
            }
            else
            {
                return ValidationProblem(ModelState);
            }
        }

        [HttpPatch]
        [Route("{orderId:guid}/items")]
        [ProducesResponseType(typeof(OrderItem), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PatchOrderItem(Guid? orderId, OrderItem model)
        {
            if (!orderId.HasValue)
                throw new ArgumentNullException(nameof(orderId));

            if (ModelState.IsValid)
            {
                model.OrderId = orderId;
                await _orderItemService.Update(model);
                return Ok(model);
            }
            else
            {
                return ValidationProblem(ModelState);
            }
        }

        [HttpDelete]
        [Route("{orderId:guid}/items/{id:guid}")]
        [ProducesResponseType(typeof(OrderItem), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteOrderItem(Guid? orderId, Guid id)
        {
            if (!orderId.HasValue)
                throw new ArgumentNullException(nameof(orderId));

            var orderItem = await _orderItemService.GetById(id);
            if (orderItem == null)
                throw new ArgumentNullException("Order item not found");

            _orderItemService.Delete(orderItem);
            return Ok();
        }
        [HttpPatch]
        [Route("{orderId:guid}/changestatus")]
        public async Task<IActionResult> ChangeStatus(Guid orderId, OrderStatusEnum newStatus, Guid displayManagerId, PaymentTypeEnum? paymentType)
        {
            var result = await _orderService.ChangeStatus(orderId, newStatus, displayManagerId, paymentType);
            return Ok(result);
        }
    }
}