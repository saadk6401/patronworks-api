﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Core;
using Patronworks.Core.ViewModel;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Model.Catalog;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/category")]
    [ApiController]
    [Authorize]
    public class CategoryController : BaseApiController<Category>
    {
        private readonly ICategoryService _categoryService;
        private readonly ICategoryPictureService _categoryPictureService;
        private readonly IImportService _importService;
        public CategoryController(ICategoryService categoryService, ICategoryPictureService categoryPictureService, IImportService importService) : base(categoryService)
        {
            _categoryService = categoryService;
            _categoryPictureService = categoryPictureService;
            _importService = importService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<Category>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<Category>), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get([FromQuery] SieveModel sieveModel, string customInclude = "")
        {
            var model = await _service.LoadAll(sieveModel, true);
            return Ok(model);
        }

        [HttpGet]
        [Route("{id:guid}")]
        [ProducesResponseType(typeof(Category), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Get(Guid id, string customInclude = "")
        {
            var model = await _service.GetById(id, true);
            return Ok(model);
        }


        [HttpPost]
        [ProducesResponseType(typeof(Category), (int)HttpStatusCode.OK)]
        public override async Task<IActionResult> Post(Category model)
        {
            return await base.Post(model);
        }

        [HttpGet]
        [Route("{categoryId:guid}/picture")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPicture(Guid categoryId)
        {
            var picture = await _categoryPictureService.DownloadPicture(categoryId);
            if (picture == null)
                return NotFound();
            var extension = MimeTypeMap.GetExtension(picture.MimeType);
            Response.Headers.Add("Content-Disposition", new ContentDisposition
            {
                FileName = $"Category_{categoryId.ToString()}_Media{extension}",
                Inline = true // false = prompt the user for downloading; true = browser to try to show the file inline
            }.ToString());
            return File(picture.PictureBinary, picture.MimeType);
        }

        [HttpPost]
        [Route("{categoryId:guid}/picture")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostPicture(Guid categoryId, IFormFile file)
        {
            var stream = file.OpenReadStream();
            await _categoryPictureService.UploadPicture(categoryId, stream, file.ContentType);
            return Ok();
        }

        [HttpPatch]
        [Route("{categoryId:guid}/picture")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PatchPicture(Guid categoryId, IFormFile file)
        {
            var stream = file.OpenReadStream();
            await _categoryPictureService.UploadPicture(categoryId, stream, file.ContentType);
            return Ok();
        }

        [HttpDelete]
        [Route("{categoryId:guid}/picture")]
        [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeletePicture(Guid categoryId)
        {
            await _categoryPictureService.DeletePicture(categoryId);
            return Ok();
        }

        [Route("importcategories")]
        [HttpPost]
        public async Task<IActionResult> ImportCategories(IFormFile xlsxFile, int sheetIndex = 0)
        {
            var result = await _importService.ImportCategories(xlsxFile, sheetIndex);
            var textResult = $"Imported {result} categories";
            return Ok(textResult);
        }
    }
}