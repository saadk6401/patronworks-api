﻿using Microsoft.AspNetCore.Mvc;
using Patronworks.Core.Extensions;
using Patronworks.Domain.Model.Orders;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    public partial class OrderController
    {
        [HttpGet]
        [Route("table/{tableId:guid}")]
        [ProducesResponseType(typeof(Order), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetForTable(Guid tableId)
        {
            //var model = await _service.GetByProperty("tableId", tableId, true);

            var sieveModel = new SieveModel();
            sieveModel.AddFilter("tableId", tableId);
            sieveModel.AddFilter("ParentOrderId", "null");
            var parentOrder = await _orderService.GetByFilters(sieveModel);
            if (parentOrder != null)
            {
                sieveModel = new SieveModel();
                sieveModel.AddFilter("ItemsForParentOrderId", parentOrder.Id);
                var orderItems = (await _orderItemService.LoadAll(sieveModel))?.Data;//.ToList();

                //parentOrder.OrderItems = orderItems;
                return Ok(orderItems);
            }
            else
            {
                return Ok();
            }
        }

        [HttpPost]
        [Route("table/{tableId:guid}")]
        [ProducesResponseType(typeof(OrderItem), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostOrderItemsForTable(Guid? tableId, List<OrderItem> models)
        {
            var data = await _orderService.AddOrderItemsForTable(tableId, models);
            return Ok(data);
        }

        [HttpPost]
        [Route("table/{tableId:guid}/buffet/beginround")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> BeginBuffetRound(Guid? tableId)
        {
            var data = await _orderService.BeginBuffetRound(tableId);
            return Ok(data);
        }

        [HttpPost]
        [Route("table/{tableId:guid}/buffet/additems")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostBuffetOrderItemsForTable(Guid? tableId, List<OrderItem> models)
        {
            var data = await _orderService.AddTempOrderItemForTable(tableId, models);
            if (data)
            {
                return Ok(data);
            }
            else
            {
                ModelState.AddModelError("round_not_begin", "The round has no begin round");
                return ValidationProblem(ModelState);
            }
        }

        [HttpPost]
        [Route("table/{tableId:guid}/buffet/cancelbuffetround")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CancelBuffetRound(Guid? tableId)
        {
            var data = await _orderService.CancelBuffetRound(tableId);
            return Ok(data);
        }

        [HttpPost]
        [Route("table/{tableId:guid}/buffet/startbuffetround")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> StartBuffetRound(Guid? tableId)
        {
            var data = await _orderService.StartBuffetRound(tableId);
            return Ok(data);
        }

        [HttpPost]
        [Route("table/{tableId:guid}/opentable")]
        [ProducesResponseType(typeof(OrderItem), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> OpenTable(Guid tableId)
        {
            var data = await _orderService.OpenTable(tableId);
            return Ok(data);
        }

        [HttpPost]
        [Route("table/{tableId:guid}/notification")]
        [ProducesResponseType(typeof(OrderItem), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostTableNotification(Guid? tableId, string notification)
        {
            return Ok(await _orderService.AddTableNotification(tableId, notification));
        }

    }
}
