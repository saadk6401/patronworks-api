﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Patronworks.Api.Extenstions;
using Patronworks.Api.ViewModels;
using Patronworks.Core.Filters;
using Patronworks.Domain.Interfaces.Services;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Patronworks.Api.Controllers
{
    [Route("api/v1/menu")]
    [ApiController]
    [Authorize]
    public class MenuController : ControllerBase
    {
        private readonly IMenuService _menuService;
        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<MenuViewModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var model = await _menuService.GetMenu();
            return Ok(model.ToViewModel());
        }


    }
}