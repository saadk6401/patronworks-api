﻿using Patronworks.Api.ViewModels;
using Patronworks.Domain.Model.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Patronworks.Api.Extenstions
{
    public static class MenuExtensions
    {
        public static IEnumerable<MenuViewModel> ToViewModel(this List<Category> categories)
        {
            if (categories != null)
            {
                return categories.Where(f => f.Active).Select(c => c.ToViewModel());
            }
            return new List<MenuViewModel>();
        }
        public static MenuViewModel ToViewModel(this Category category)
        {
            var menuViewModel = new MenuViewModel
            {
                Id = category.Id.Value,
                Name = category.Name,
                CategoryType = category.CategoryType,
                PictureId = category.CategoryPictures.FirstOrDefault()?.PictureId,
                Products = category.Products.ToMenuItemViewModel(category.DisplayManagerId),
                ParentId = category.ParentId,
                Extradata = category.Extradata,
                LampixIcon = category.LampixIcon,
                Translations = category.Translations,
                ProductTilesNo = category.ProductTilesNo,
                ShowPictures = category.ShowPictures,
                Order = category.Order,
                Categories = category?.Children.ToViewModel()
            };
            return menuViewModel;
        }

        private static List<MenuItemViewModel> ToMenuItemViewModel(this List<Product> products, Guid? displayId)
        {
            if (products != null)
                return products.Where(f => f.Active).Select(c => c.ToMenuItemViewModel(displayId)).ToList();
            else
                return new List<MenuItemViewModel>();
        }
        private static MenuItemViewModel ToMenuItemViewModel(this Product product, Guid? displayId)
        {
            var menuItemViewModel = new MenuItemViewModel
            {
                Id = product.Id.Value,
                FullDescription = product.FullDescription,
                Name = product.Name,
                Price = product.Price,
                PriceExclTax = product.GetPriceExclTax(),
                TaxValue = product.InHouseTaxValue,
                ShortDescription = product.ShortDescription,
                PictureId = product.ProductPictures.FirstOrDefault()?.PictureId,
                DisplayManagerId = displayId,
                Extradata = product.Extradata,
                Translations = product.Translations,
                Order = product.Order,
                Code = product.BarCode

            };
            return menuItemViewModel;
        }
        private static decimal GetPriceExclTax(this Product product)
        {
            var price = product.Price;
            var taxValue = product.InHouseTaxValue;
            if (taxValue.HasValue)
            {
                decimal priceExclTax;
                priceExclTax = Decimal.Round((price * 100) / (taxValue.Value + 100), 2);
                return priceExclTax;
            }
            return price;
        }
    }
}
