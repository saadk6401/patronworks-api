﻿using Patronworks.Domain.Model.Orders;
using System.Collections.Generic;
using System.Linq;

namespace Patronworks.Api.Extenstions
{
    public static class OrderExtensions
    {
        public static List<OrderItem> UpdateOrderItems(this Order order, List<OrderItem> orderItems)
        {
            var existingIds = order.OrderItems.Select(c => c.Id.Value).ToList();
            if (existingIds.Count() > 0)
            {
                var modifiedOrderItems = orderItems.Where(f => f.Id.HasValue && existingIds.Contains(f.Id.Value)).ToList();
                foreach (var modifiedOrderItem in modifiedOrderItems)
                {
                    var orderItem = order.OrderItems.FirstOrDefault(f => f.Id == modifiedOrderItem.Id);
                    modifiedOrderItem.OrderId = order.Id;
                    modifiedOrderItem.PrintedQty = orderItem.PrintedQty;
                }
                return modifiedOrderItems;
            }
            return new List<OrderItem>();
            //var newOrderItems = orderItems.Where(f => !f.Id.HasValue);
            //if (newOrderItems?.Count() > 0)
            //{
            //    order.OrderItems.AddRange(newOrderItems);
            //}
            //var modifiedOrderItems = orderItems.Where(f => f.Id.HasValue);
            //foreach (var modifiedOrderItem in modifiedOrderItems)
            //{
            //    var orderItem = order.OrderItems.FirstOrDefault(f => f.Id == modifiedOrderItem.Id);
            //    orderItem.Quantity = modifiedOrderItem.Quantity;
            //}
        }
    }
}
