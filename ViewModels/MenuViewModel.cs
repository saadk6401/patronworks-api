﻿using Newtonsoft.Json;
using Patronworks.Core.Enums;
using System;
using System.Collections.Generic;
using System.Dynamic;

namespace Patronworks.Api.ViewModels
{
    public class MenuViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CategoryType CategoryType { get; set; }
        public Guid? PictureId { get; set; }
        public Guid? ParentId { get; set; }
        public List<MenuItemViewModel> Products { get; set; } = new List<MenuItemViewModel>();
        [JsonProperty("extraData")]
        public ExpandoObject Extradata { get; set; } = new ExpandoObject();
        public string LampixIcon { get; set; }

        [JsonProperty("translations")]
        public ExpandoObject Translations { get; set; } = new ExpandoObject();

        public int Order { get; set; } = 0;

        [JsonProperty("showPictures")]
        public bool ShowPictures { get; set; } = true;

        [JsonProperty("productTilesNo")]
        public int ProductTilesNo { get; set; } = 3;

        public IEnumerable<MenuViewModel> Categories { get; set; } = new List<MenuViewModel>();
    }

    public class MenuItemViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }
        [JsonProperty("extraData")]
        public ExpandoObject Extradata { get; set; } = new ExpandoObject();
        public decimal Price { get; set; }
        public decimal PriceExclTax { get; set; }
        public decimal? TaxValue { get; set; }
        public Guid? PictureId { get; set; }
        public Guid? DisplayManagerId { get; set; }

        [JsonProperty("translations")]
        public ExpandoObject Translations { get; set; } = new ExpandoObject();

        public int Order { get; set; } = 0;

        public string Code { get; set; }


    }
}
