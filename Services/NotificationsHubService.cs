﻿using Microsoft.AspNetCore.SignalR;
using Patronworks.Api.Hubs;
using Patronworks.Core.Events;
using Patronworks.Core.Models;
using Patronworks.Services.Events;
using System;
using System.Threading.Tasks;

namespace Patronworks.Api.Services
{
    public class NotificationsHubService :
        IConsumer<EntityInsertedEvent<PatronworksNotification>>,
        IConsumer<EntityUpdatedEvent<PatronworksNotification>>
    {
        private readonly IHubContext<NotificationsHub> _hubContext;
        public NotificationsHubService(IHubContext<NotificationsHub> hubContext)
        {
            _hubContext = hubContext ?? throw new ArgumentNullException(nameof(hubContext));
        }
        public async Task HandleEvent(EntityInsertedEvent<PatronworksNotification> eventMessage)
        {
            await _hubContext.Clients.All.SendAsync("Notification", eventMessage.Entity);

        }

        public async Task HandleEvent(EntityUpdatedEvent<PatronworksNotification> eventMessage)
        {
            await _hubContext.Clients.All.SendAsync("Notification", eventMessage.Entity);
        }
    }
}
