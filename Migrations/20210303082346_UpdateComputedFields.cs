﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Patronworks.Api.Migrations
{
    public partial class UpdateComputedFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "LineValueTax",
                table: "OrderItems",
                type: "numeric",
                nullable: false,
                computedColumnSql: "(\"Quantity\"*\"PriceInclTax\")-(\"Quantity\"*\"PriceExclTax\")",
                stored: true,
                oldClrType: typeof(decimal),
                oldType: "numeric",
                oldComputedColumnSql: "(\"Quantity\"*\"PriceInclTax\")-(\"Quantity\"*\"PriceExclTax\")",
                oldStored: null);

            migrationBuilder.AlterColumn<decimal>(
                name: "LineValueInclTax",
                table: "OrderItems",
                type: "numeric",
                nullable: false,
                computedColumnSql: "\"Quantity\"*\"PriceInclTax\"",
                stored: true,
                oldClrType: typeof(decimal),
                oldType: "numeric",
                oldComputedColumnSql: "\"Quantity\"*\"PriceInclTax\"",
                oldStored: null);

            migrationBuilder.AlterColumn<decimal>(
                name: "LineValueExclTax",
                table: "OrderItems",
                type: "numeric",
                nullable: false,
                computedColumnSql: "\"Quantity\"*\"PriceExclTax\"",
                stored: true,
                oldClrType: typeof(decimal),
                oldType: "numeric",
                oldComputedColumnSql: "\"Quantity\"*\"PriceExclTax\"",
                oldStored: null);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "LineValueTax",
                table: "OrderItems",
                type: "numeric",
                nullable: false,
                computedColumnSql: "(\"Quantity\"*\"PriceInclTax\")-(\"Quantity\"*\"PriceExclTax\")",
                oldClrType: typeof(decimal),
                oldType: "numeric",
                oldComputedColumnSql: "(\"Quantity\"*\"PriceInclTax\")-(\"Quantity\"*\"PriceExclTax\")");

            migrationBuilder.AlterColumn<decimal>(
                name: "LineValueInclTax",
                table: "OrderItems",
                type: "numeric",
                nullable: false,
                computedColumnSql: "\"Quantity\"*\"PriceInclTax\"",
                oldClrType: typeof(decimal),
                oldType: "numeric",
                oldComputedColumnSql: "\"Quantity\"*\"PriceInclTax\"");

            migrationBuilder.AlterColumn<decimal>(
                name: "LineValueExclTax",
                table: "OrderItems",
                type: "numeric",
                nullable: false,
                computedColumnSql: "\"Quantity\"*\"PriceExclTax\"",
                oldClrType: typeof(decimal),
                oldType: "numeric",
                oldComputedColumnSql: "\"Quantity\"*\"PriceExclTax\"");
        }
    }
}
