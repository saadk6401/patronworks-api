﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Patronworks.Api.Migrations
{
    public partial class PaymentGTypesMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "DefaultPayment",
                table: "PaymentType",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "PaymentType",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "PaymentsGTypeId",
                table: "PaymentType",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ShowCaption",
                table: "PaymentType",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "PaymentsGType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Code = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentsGType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PaymentType_PaymentsGTypeId",
                table: "PaymentType",
                column: "PaymentsGTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaymentType_PaymentsGType_PaymentsGTypeId",
                table: "PaymentType",
                column: "PaymentsGTypeId",
                principalTable: "PaymentsGType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaymentType_PaymentsGType_PaymentsGTypeId",
                table: "PaymentType");

            migrationBuilder.DropTable(
                name: "PaymentsGType");

            migrationBuilder.DropIndex(
                name: "IX_PaymentType_PaymentsGTypeId",
                table: "PaymentType");

            migrationBuilder.DropColumn(
                name: "DefaultPayment",
                table: "PaymentType");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "PaymentType");

            migrationBuilder.DropColumn(
                name: "PaymentsGTypeId",
                table: "PaymentType");

            migrationBuilder.DropColumn(
                name: "ShowCaption",
                table: "PaymentType");
        }
    }
}
