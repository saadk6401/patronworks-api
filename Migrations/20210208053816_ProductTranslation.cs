﻿using Microsoft.EntityFrameworkCore.Migrations;
using System.Dynamic;

namespace Patronworks.Api.Migrations
{
    public partial class ProductTranslation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ExpandoObject>(
                name: "Translations",
                table: "Products",
                type: "jsonb",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Translations",
                table: "Products");
        }
    }
}
