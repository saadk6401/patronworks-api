﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Patronworks.Api.Migrations
{
    public partial class PatronworksDeviceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tables_LampixDevices_LampixDeviceId",
                table: "Tables");

            migrationBuilder.DropIndex(
                name: "IX_Tables_LampixDeviceId",
                table: "Tables");

            migrationBuilder.DropColumn(
                name: "LampixDeviceId",
                table: "Tables");

            migrationBuilder.AlterColumn<string>(
                name: "DeviceId",
                table: "LampixDevices",
                type: "text",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Settings",
                table: "LampixDevices",
                type: "jsonb",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TableId",
                table: "LampixDevices",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LampixDevices_TableId",
                table: "LampixDevices",
                column: "TableId");

            migrationBuilder.AddForeignKey(
                name: "FK_LampixDevices_Tables_TableId",
                table: "LampixDevices",
                column: "TableId",
                principalTable: "Tables",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LampixDevices_Tables_TableId",
                table: "LampixDevices");

            migrationBuilder.DropIndex(
                name: "IX_LampixDevices_TableId",
                table: "LampixDevices");

            migrationBuilder.DropColumn(
                name: "Settings",
                table: "LampixDevices");

            migrationBuilder.DropColumn(
                name: "TableId",
                table: "LampixDevices");

            migrationBuilder.AddColumn<Guid>(
                name: "LampixDeviceId",
                table: "Tables",
                type: "uuid",
                nullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DeviceId",
                table: "LampixDevices",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tables_LampixDeviceId",
                table: "Tables",
                column: "LampixDeviceId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Tables_LampixDevices_LampixDeviceId",
                table: "Tables",
                column: "LampixDeviceId",
                principalTable: "LampixDevices",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
