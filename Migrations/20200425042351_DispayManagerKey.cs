﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Patronworks.Api.Migrations
{
    public partial class DispayManagerKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayKey",
                table: "Display",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayKey",
                table: "Display");
        }
    }
}
