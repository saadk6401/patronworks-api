﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using System;

namespace Patronworks.Api.Migrations
{
    public partial class Order : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    OperatorId = table.Column<Guid>(nullable: true),
                    TableId = table.Column<Guid>(nullable: true),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    OrderNo = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Points = table.Column<decimal>(nullable: false),
                    OrderValueExclTax = table.Column<decimal>(nullable: false),
                    OrderValueTax = table.Column<decimal>(nullable: false),
                    OrderValueInclTax = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Tables_TableId",
                        column: x => x.TableId,
                        principalTable: "Tables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    ProductId = table.Column<Guid>(nullable: true),
                    Points = table.Column<decimal>(nullable: false),
                    TaxValue = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<decimal>(nullable: false),
                    PriceExclTax = table.Column<decimal>(nullable: false),
                    PriceInclTax = table.Column<decimal>(nullable: false),
                    LineValueExclTax = table.Column<decimal>(nullable: false, computedColumnSql: "\"Quantity\"*\"PriceExclTax\"", stored: true),
                    LineValueTax = table.Column<decimal>(nullable: false, computedColumnSql: "(\"Quantity\"*\"PriceInclTax\")-(\"Quantity\"*\"PriceExclTax\")", stored: true),
                    LineValueInclTax = table.Column<decimal>(nullable: false, computedColumnSql: "\"Quantity\"*\"PriceInclTax\"", stored: true),
                    Text = table.Column<string>(nullable: true),
                    Units = table.Column<decimal>(nullable: false),
                    DetailType = table.Column<int>(nullable: false),
                    OrderId = table.Column<Guid>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    PrintedQty = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderItems_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderItems_OrderItems_ParentId",
                        column: x => x.ParentId,
                        principalTable: "OrderItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrderItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_OrderId",
                table: "OrderItems",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_ParentId",
                table: "OrderItems",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_ProductId",
                table: "OrderItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_TableId",
                table: "Orders",
                column: "TableId");

            var sql = @"CREATE OR REPLACE  FUNCTION public.update_order_value()" + Environment.NewLine +
                        @"    RETURNS trigger" + Environment.NewLine +
                        @"AS $$" + Environment.NewLine +
                        @"BEGIN" + Environment.NewLine +
                        @"Update ""Orders""" + Environment.NewLine +
                        @"Set " + Environment.NewLine +
                        @"""OrderValueExclTax""=total_val.total_line_excl_tax, " + Environment.NewLine +
                        @"""OrderValueTax""=total_val.total_line_tax," + Environment.NewLine +
                        @"""OrderValueInclTax""=total_val.total_line_incl_tax" + Environment.NewLine +
                        @"From (" + Environment.NewLine +
                        @"Select Sum(""LineValueInclTax"") total_line_incl_tax,Sum(""LineValueExclTax"") total_line_excl_tax,Sum(""LineValueTax"") total_line_tax, ""OrderId"" " + Environment.NewLine +
                        @"From  ""OrderItems""" + Environment.NewLine +
                        @"Where ""OrderItems"".""OrderId""=NEW.""OrderId""" + Environment.NewLine +
                        @"GROUP BY ""OrderId"") total_val" + Environment.NewLine +
                        @"Where ""Id""=NEW.""OrderId"";" + Environment.NewLine +
                        @"RETURN NEW;" + Environment.NewLine +
                        @"END;" + Environment.NewLine +
                        @"$$" + Environment.NewLine +
                        @"LANGUAGE 'plpgsql'; ";
            migrationBuilder.Sql(sql);

            sql = @"CREATE TRIGGER update_order_value_triger" + Environment.NewLine +
                    @"    AFTER INSERT OR DELETE OR UPDATE " + Environment.NewLine +
                    @"    ON public.""OrderItems""" + Environment.NewLine +
                    @"    FOR EACH ROW" + Environment.NewLine +
                    @"    EXECUTE PROCEDURE public.update_order_value();";
            migrationBuilder.Sql(sql);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderItems");

            migrationBuilder.DropTable(
                name: "Orders");

            var sql = @"DROP FUNCTION  IF EXISTS update_order_value";
            migrationBuilder.Sql(sql);
        }
    }
}
