﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Patronworks.Api.Migrations
{
    public partial class OrderStatusAndDisplayManagerOrdetItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrderStatus",
                table: "Orders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "DisplayManagerId",
                table: "OrderItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderItems_DisplayManagerId",
                table: "OrderItems",
                column: "DisplayManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderItems_Display_DisplayManagerId",
                table: "OrderItems",
                column: "DisplayManagerId",
                principalTable: "Display",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderItems_Display_DisplayManagerId",
                table: "OrderItems");

            migrationBuilder.DropIndex(
                name: "IX_OrderItems_DisplayManagerId",
                table: "OrderItems");

            migrationBuilder.DropColumn(
                name: "OrderStatus",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "DisplayManagerId",
                table: "OrderItems");
        }
    }
}
