﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Patronworks.Api.Migrations
{
    public partial class CorrectPaymentRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Check_Payment_PaymentId",
                table: "Check");

            migrationBuilder.DropIndex(
                name: "IX_Check_PaymentId",
                table: "Check");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "Check");

            migrationBuilder.CreateSequence<int>(
                name: "CheckNumbers");

            migrationBuilder.AddColumn<Guid>(
                name: "CheckId",
                table: "Payment",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CheckNo",
                table: "Check",
                nullable: false,
                defaultValueSql: "nextval('\"CheckNumbers\"')",
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_CheckId",
                table: "Payment",
                column: "CheckId");

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_Check_CheckId",
                table: "Payment",
                column: "CheckId",
                principalTable: "Check",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payment_Check_CheckId",
                table: "Payment");

            migrationBuilder.DropIndex(
                name: "IX_Payment_CheckId",
                table: "Payment");

            migrationBuilder.DropSequence(
                name: "CheckNumbers");

            migrationBuilder.DropColumn(
                name: "CheckId",
                table: "Payment");

            migrationBuilder.AlterColumn<int>(
                name: "CheckNo",
                table: "Check",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldDefaultValueSql: "nextval('\"CheckNumbers\"')");

            migrationBuilder.AddColumn<Guid>(
                name: "PaymentId",
                table: "Check",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Check_PaymentId",
                table: "Check",
                column: "PaymentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Check_Payment_PaymentId",
                table: "Check",
                column: "PaymentId",
                principalTable: "Payment",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
