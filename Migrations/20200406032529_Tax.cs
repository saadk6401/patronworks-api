﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Patronworks.Api.Migrations
{
    public partial class Tax : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TaxValue",
                table: "Products");

            migrationBuilder.AddColumn<Guid>(
                name: "InHouseTaxId",
                table: "Products",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TakeawayTaxId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Taxes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    TaxValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Taxes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_InHouseTaxId",
                table: "Products",
                column: "InHouseTaxId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_TakeawayTaxId",
                table: "Products",
                column: "TakeawayTaxId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Taxes_InHouseTaxId",
                table: "Products",
                column: "InHouseTaxId",
                principalTable: "Taxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Taxes_TakeawayTaxId",
                table: "Products",
                column: "TakeawayTaxId",
                principalTable: "Taxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Taxes_InHouseTaxId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Taxes_TakeawayTaxId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "Taxes");

            migrationBuilder.DropIndex(
                name: "IX_Products_InHouseTaxId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_TakeawayTaxId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "InHouseTaxId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "TakeawayTaxId",
                table: "Products");

            migrationBuilder.AddColumn<decimal>(
                name: "TaxValue",
                table: "Products",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
