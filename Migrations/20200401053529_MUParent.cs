﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Patronworks.Api.Migrations
{
    public partial class MUParent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "ParentId",
                table: "MUs",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.CreateIndex(
                name: "IX_MUs_ParentId",
                table: "MUs",
                column: "ParentId");

            migrationBuilder.AddForeignKey(
                name: "FK_MUs_MUs_ParentId",
                table: "MUs",
                column: "ParentId",
                principalTable: "MUs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MUs_MUs_ParentId",
                table: "MUs");

            migrationBuilder.DropIndex(
                name: "IX_MUs_ParentId",
                table: "MUs");

            migrationBuilder.AlterColumn<Guid>(
                name: "ParentId",
                table: "MUs",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);
        }
    }
}
