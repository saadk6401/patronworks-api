﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Patronworks.Api.Migrations
{
    public partial class CheckView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var checksView = @"CREATE OR REPLACE VIEW public.""CheckView""
                             AS
							 SELECT ""Check"".""Id"",
                            ""Check"".""CheckNo"",
                            concat(users.""FirstName"", '  ', users.""LastName"") AS ""Operator"",
                            ""Check"".""ValueExclTax"" AS ""SubTotal"",
                            ""Check"".""ValueTax"" AS ""Tax"",
                            ""Check"".""ValueInclTax"" AS ""Ammount"",
                            ""Check"".""CheckDate"",
                            ""Tables"".""Name"" AS ""Table"",
	                        ""Payment"".""Payments""
                           FROM ""Check""
                             JOIN (Select ""CheckId"",string_agg(""PaymentType"".""Name"",', ') as ""Payments"" from ""Payment""
	                         JOIN ""PaymentType"" ON ""Payment"".""PaymentTypeId"" = ""PaymentType"".""Id"" Group By ""CheckId"")""Payment"" ON ""Payment"".""CheckId"" =""Check"".""Id"" 
                              JOIN ""Tables"" ON ""Check"".""TableId"" = ""Tables"".""Id""
                             JOIN users ON ""Check"".""OperatorId""::text = users.""Id""
                          ORDER BY ""Check"".""CheckDate"" DESC;";
            migrationBuilder.Sql(checksView);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
