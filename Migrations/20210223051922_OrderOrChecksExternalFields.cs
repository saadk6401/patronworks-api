﻿using Microsoft.EntityFrameworkCore.Migrations;
using System.Dynamic;

namespace Patronworks.Api.Migrations
{
    public partial class OrderOrChecksExternalFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ExpandoObject>(
                name: "ExternalData",
                table: "Orders",
                type: "jsonb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExternalId",
                table: "Orders",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<ExpandoObject>(
                name: "ExternalData",
                table: "Check",
                type: "jsonb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExternalId",
                table: "Check",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExternalData",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ExternalId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ExternalData",
                table: "Check");

            migrationBuilder.DropColumn(
                name: "ExternalId",
                table: "Check");
        }
    }
}
