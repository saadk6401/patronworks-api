﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Patronworks.Api.Migrations
{
    public partial class CategoryParents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var update = @"CREATE OR REPLACE VIEW public.""CategoryParentsView""
                         AS
                         WITH RECURSIVE parents AS (
                                 SELECT ""Categories"".""Id"",
                                    ""Categories"".""Name"",
                                    0 AS number_of_ancestors,
                                    ARRAY[""Categories"".""Name""] AS ancestry,
	                                ARRAY[""Categories"".""Id""] AS ancestryids,
	                               NULL::uuid AS parent
                                   FROM ""Categories""
                                  WHERE ""Categories"".""ParentId"" IS NULL
                                UNION
                                 SELECT child.""Id"",
                                    child.""Name"",
                                    p.number_of_ancestors + 1 AS ancestry_size,
                                    array_append(p.ancestry, child.""Name"") AS ancestry,
	                                array_append(p.ancestryids, child.""Id"") AS ancestryids,	 
                                    child.""ParentId""
                                   FROM ""Categories"" child
                                     JOIN parents p ON p.""Id"" = child.""ParentId""
                                )
                         SELECT parents.""Id"",
                            array_to_string(parents.ancestry, '->'::text) AS ""Parents"",
	                        array_to_string(parents.ancestryids, ','::text)  AS ""ParentsIds""
                           FROM parents;";
            migrationBuilder.Sql(update);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
