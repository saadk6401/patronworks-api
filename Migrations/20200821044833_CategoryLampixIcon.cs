﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Patronworks.Api.Migrations
{
    public partial class CategoryLampixIcon : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LampixIcon",
                table: "Categories",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LampixIcon",
                table: "Categories");
        }
    }
}
