﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Patronworks.Api.Migrations
{
    public partial class ProductOrderTrigger : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sql = @"CREATE OR REPLACE FUNCTION  public.set_product_order()" + Environment.NewLine +
                    @"RETURNS trigger   " + Environment.NewLine +
                    @"AS $$" + Environment.NewLine +
                    @"BEGIN" + Environment.NewLine +
                    @"Update ""Products""" + Environment.NewLine +
                    @"Set " + Environment.NewLine +
                    @"""Order""=order_by_cat.next_order" + Environment.NewLine +
                    @"From (" + Environment.NewLine +
                    @"Select Count(*) next_order From  ""Products""" + Environment.NewLine +
                    @"Where ""CategoryId""=NEW.""CategoryId""" + Environment.NewLine +
                    @") order_by_cat" + Environment.NewLine +
                    @"Where ""Id""=NEW.""Id""" + Environment.NewLine +
                    @"AND NEW.""Order""=0;" + Environment.NewLine +
                    @"RETURN NEW;" + Environment.NewLine +
                    @"END;" + Environment.NewLine +
                    @"$$" + Environment.NewLine +
                    @"LANGUAGE 'plpgsql';";
            migrationBuilder.Sql(sql);

            sql = @"DROP TRIGGER IF EXISTS update_order_value_triger ON public.""OrderItems"";";
            migrationBuilder.Sql(sql);

            sql = @"CREATE TRIGGER update_order_triger" + Environment.NewLine +
                    @"AFTER INSERT OR DELETE OR UPDATE " + Environment.NewLine +
                    @"ON public.""Products""" + Environment.NewLine +
                    @"FOR EACH ROW" + Environment.NewLine +
                    @"EXECUTE PROCEDURE public.set_product_order();";
            migrationBuilder.Sql(sql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
