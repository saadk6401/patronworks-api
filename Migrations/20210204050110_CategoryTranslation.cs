﻿using Microsoft.EntityFrameworkCore.Migrations;
using Patronworks.Core.Models.Translate;
using System.Collections.Generic;

namespace Patronworks.Api.Migrations
{
    public partial class CategoryTranslation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<List<EntityTranslation>>(
                name: "Translations",
                table: "Categories",
                type: "jsonb",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Translations",
                table: "Categories");
        }
    }
}
