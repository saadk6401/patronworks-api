﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Patronworks.Api.Migrations
{
    public partial class DispayManager : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DisplayManagerId",
                table: "Categories",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Display",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    DefaultSort = table.Column<int>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    SystemDisplay = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Display", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Categories_DisplayManagerId",
                table: "Categories",
                column: "DisplayManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_Display_DisplayManagerId",
                table: "Categories",
                column: "DisplayManagerId",
                principalTable: "Display",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Categories_Display_DisplayManagerId",
                table: "Categories");

            migrationBuilder.DropTable(
                name: "Display");

            migrationBuilder.DropIndex(
                name: "IX_Categories_DisplayManagerId",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "DisplayManagerId",
                table: "Categories");
        }
    }
}
