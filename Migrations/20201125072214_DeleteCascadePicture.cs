﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Patronworks.Api.Migrations
{
    public partial class DeleteCascadePicture : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryPictures_Categories_CategoryId",
                table: "CategoryPictures");


            migrationBuilder.DropForeignKey(
                name: "FK_ProductPicture_Products_ProductId",
                table: "ProductPicture");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryPictures_Categories_CategoryId",
                table: "CategoryPictures",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductPicture_Products_ProductId",
                table: "ProductPicture",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);


            migrationBuilder.DropForeignKey(
              name: "FK_ProductPicture_Pictures_PictureId",
              table: "ProductPicture");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductPicture_Pictures_PictureId",
                table: "ProductPicture",
                column: "PictureId",
                principalTable: "Pictures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);



            migrationBuilder.DropForeignKey(
              name: "FK_CategoryPictures_Pictures_PictureId",
              table: "CategoryPictures");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryPictures_Pictures_PictureId",
                table: "CategoryPictures",
                column: "PictureId",
                principalTable: "Pictures",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryPictures_Categories_CategoryId",
                table: "CategoryPictures");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductPicture_Products_ProductId",
                table: "ProductPicture");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryPictures_Categories_CategoryId",
                table: "CategoryPictures",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductPicture_Products_ProductId",
                table: "ProductPicture",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
