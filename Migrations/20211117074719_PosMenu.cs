﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Patronworks.Api.Migrations
{
    public partial class PosMenu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PosMenuSizes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Columns = table.Column<int>(type: "integer", nullable: false),
                    Rows = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PosMenuSizes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PosMenus",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Comments = table.Column<string>(type: "text", nullable: true),
                    Active = table.Column<bool>(type: "boolean", nullable: false),
                    PosMenuSizeId = table.Column<Guid>(type: "uuid", nullable: true),
                    FirstColumnFixed = table.Column<bool>(type: "boolean", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PosMenus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PosMenus_PosMenuSizes_PosMenuSizeId",
                        column: x => x.PosMenuSizeId,
                        principalTable: "PosMenuSizes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PosMenuItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Row = table.Column<int>(type: "integer", nullable: false),
                    Column = table.Column<int>(type: "integer", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: true),
                    ProductId = table.Column<Guid>(type: "uuid", nullable: true),
                    PosMenuId = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PosMenuItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PosMenuItems_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PosMenuItems_PosMenus_PosMenuId",
                        column: x => x.PosMenuId,
                        principalTable: "PosMenus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PosMenuItems_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PosMenuItems_CategoryId",
                table: "PosMenuItems",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_PosMenuItems_PosMenuId",
                table: "PosMenuItems",
                column: "PosMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_PosMenuItems_ProductId",
                table: "PosMenuItems",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_PosMenus_PosMenuSizeId",
                table: "PosMenus",
                column: "PosMenuSizeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PosMenuItems");

            migrationBuilder.DropTable(
                name: "PosMenus");

            migrationBuilder.DropTable(
                name: "PosMenuSizes");
        }
    }
}
