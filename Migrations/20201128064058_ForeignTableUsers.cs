﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Patronworks.Api.Migrations
{
    public partial class ForeignTableUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var users = @"CREATE FOREIGN TABLE  IF NOT EXISTS  Users (
                            ""Id"" text NOT NULL COLLATE pg_catalog.""default"",
                            ""FirstName"" text NULL COLLATE pg_catalog.""default"",
                            ""LastName"" text NULL COLLATE pg_catalog.""default""
                        )
                        SERVER identity_fs
                        OPTIONS(schema_name 'public', table_name 'User');";
            migrationBuilder.Sql(users);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
