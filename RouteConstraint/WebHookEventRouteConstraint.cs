﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Patronworks.Domain.Model;
using System;

namespace Patronworks.Api.RouteConstraint
{
    public class WebHookEventRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContext httpContext, IRouter route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
        {
            // retrieve the candidate value
            var questionnaireType = values[routeKey]?.ToString();
            // attempt to parse the candidate to the required Enum type, and return the result
            var res = Enum.TryParse(typeof(WebHookEvent), questionnaireType, true, out Object result1);
            var ss = Enum.TryParse(questionnaireType, true, out WebHookEvent result);
            return ss;
        }
    }
}
