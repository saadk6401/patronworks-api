﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Patronworks.Core.Extensions;
using Patronworks.Core.Infrastructure;
using System;

namespace Patronworks.Api
{
    public class Startup
    {
        #region Fields

        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _hostingEnvironment;

        #endregion
        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            _configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            return services.ConfigureApplicationServices(_configuration, _hostingEnvironment, true);
        }

        public void Configure(IApplicationBuilder app)
        {
            EngineContext.Current.ConfigureRequestPipeline(app);
            app.StartEngine();
        }
    }
}