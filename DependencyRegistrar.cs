﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Patronworks.Core.Infrastructure;
using Patronworks.Core.Infrastructure.DependencyManagement;
using Patronworks.Core.Interfaces.Repository;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.Models;
using Patronworks.Core.Models.Media;
using Patronworks.Core.Repository;
using Patronworks.Core.Services;
using Patronworks.Domain.Interfaces.Repository;
using Patronworks.Domain.Interfaces.Services;
using Patronworks.Domain.Interfaces.Services.Plugins;
using Patronworks.Domain.Model.Administration;
using Patronworks.Domain.Model.Catalog;
using Patronworks.Domain.Model.Checks;
using Patronworks.Domain.Model.Modifiers;
using Patronworks.Domain.Model.Orders;
using Patronworks.Domain.Model.Payments;
using Patronworks.Domain.Services;
using Patronworks.Domain.Services.Plugins;
using Patronworks.Infrastructure;
using Patronworks.Infrastructure.Repository;

namespace Patronworks.Api
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public int Order => 1;

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, IConfiguration configuration)
        {
            builder.RegisterType<BaseService<MU>>().As<IService<MU>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<MU, PatronworksDbContext>>().As<IRepository<MU>>().InstancePerLifetimeScope();

            builder.RegisterType<CategoryService>().As<ICategoryService>().InstancePerLifetimeScope();
            builder.RegisterType<CategoryRepository>().As<ICategoryRepository>().InstancePerLifetimeScope();

            builder.RegisterType<ProductService>().As<IProductService>().InstancePerLifetimeScope();
            builder.RegisterType<ProductRepository>().As<IProductRepository>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<Picture>>().As<IService<Picture>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<Picture, PatronworksDbContext>>().As<IRepository<Picture>>().InstancePerLifetimeScope();

            builder.RegisterType<CategoryPictureService>().As<ICategoryPictureService>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<CategoryPicture, PatronworksDbContext>>().As<IRepository<CategoryPicture>>().InstancePerLifetimeScope();

            builder.RegisterType<ProductPictureService>().As<IProductPictureService>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<ProductPicture, PatronworksDbContext>>().As<IRepository<ProductPicture>>().InstancePerLifetimeScope();

            builder.RegisterType<TableService>().As<ITableService>().InstancePerLifetimeScope();
            builder.RegisterType<TableRepository>().As<ITableRepository>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<PatronworksDevice>>().As<IService<PatronworksDevice>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<PatronworksDevice, PatronworksDbContext>>().As<IRepository<PatronworksDevice>>().InstancePerLifetimeScope();

            builder.RegisterType<OrderService>().As<IOrderService>().InstancePerLifetimeScope();
            //builder.RegisterType<BaseRepository<Order, PatronworksDbContext>>().As<IRepository<Order>>().InstancePerLifetimeScope();
            builder.RegisterType<OrderRepository>().As<IRepository<Order>>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<OrderItem>>().As<IService<OrderItem>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<OrderItem, PatronworksDbContext>>().As<IRepository<OrderItem>>().InstancePerLifetimeScope();

            builder.RegisterType<MenuService>().As<IMenuService>().InstancePerLifetimeScope();
            builder.RegisterType<MenuRepository>().As<IMenuRepository>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<Tax>>().As<IService<Tax>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<Tax, PatronworksDbContext>>().As<IRepository<Tax>>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<PatronworksNotification>>().As<IService<PatronworksNotification>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<PatronworksNotification, PatronworksDbContext>>().As<IRepository<PatronworksNotification>>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<Display>>().As<IService<Display>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<Display, PatronworksDbContext>>().As<IRepository<Display>>().InstancePerLifetimeScope();

            builder.RegisterType<EFStringLocalizer<Table>>().As<IStringLocalizer<Table>>().InstancePerLifetimeScope();

            builder.RegisterType<ImportService>().As<IImportService>().InstancePerLifetimeScope();

            builder.RegisterType<CommonService>().As<ICommonService>().InstancePerLifetimeScope();
            builder.RegisterType<CommonRepository<PatronworksDbContext>>().As<ICommonRepository>().InstancePerLifetimeScope();

            builder.RegisterType<CheckService>().As<ICheckService>().InstancePerLifetimeScope();
            builder.RegisterType<CheckRepository>().As<ICheckRepository>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<CheckItem>>().As<IService<CheckItem>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<CheckItem, PatronworksDbContext>>().As<IRepository<CheckItem>>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<Payment>>().As<IService<Payment>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<Payment, PatronworksDbContext>>().As<IRepository<Payment>>().InstancePerLifetimeScope();

            builder.RegisterType<PaymentTypeService>().As<IPaymentTypeService>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<PaymentType, PatronworksDbContext>>().As<IRepository<PaymentType>>().InstancePerLifetimeScope();

            builder.RegisterType<IntegrationPluginManager>().As<IIntegrationPluginManager>().InstancePerLifetimeScope();


            builder.RegisterType<BaseService<ModifierTemplate>>().As<IService<ModifierTemplate>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<ModifierTemplate, PatronworksDbContext>>().As<IRepository<ModifierTemplate>>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<ModifierGroup>>().As<IService<ModifierGroup>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<ModifierGroup, PatronworksDbContext>>().As<IRepository<ModifierGroup>>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<ModifierItem>>().As<IService<ModifierItem>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<ModifierItem, PatronworksDbContext>>().As<IRepository<ModifierItem>>().InstancePerLifetimeScope();

            builder.RegisterType<BaseService<PaymentsGType>>().As<IService<PaymentsGType>>().InstancePerLifetimeScope();
            builder.RegisterType<BaseRepository<PaymentsGType, PatronworksDbContext>>().As<IRepository<PaymentsGType>>().InstancePerLifetimeScope();


            builder.RegisterType<WebHookService>().As<IWebHookService>().InstancePerLifetimeScope();
        }
    }
}
