﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace Patronworks.Api.Hubs
{
    //[Authorize]
    public class NotificationsHub : Hub
    {
        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }
        public override Task OnDisconnectedAsync(Exception exception)
        {
            return base.OnDisconnectedAsync(exception);
        }
    }
}
